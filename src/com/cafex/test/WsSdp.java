package com.cafex.test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * Servlet implementation class MediaServlet
 */
@ServerEndpoint("/media")
public class WsSdp {
	private static final long serialVersionUID = 1L;
	
	private static ConcurrentMap<String, Session> sessions = new ConcurrentHashMap<>();
	
	/**
     * Default constructor. 
     */
    public WsSdp() {
    }
	
	

	@OnOpen
	public void onOpen(Session session, EndpointConfig endpoint) {
	    sessions.put(session.getId(), session);
	    
	    sendIdMessage(session, "id", session.getId());
	}

	@OnMessage
	public synchronized void onMessage(Session session, String message) {
	    ObjectMapper mapper = new ObjectMapper();
	    JsonNode node;
        try {
            node = mapper.readTree(message);
            final Session peerSession = sessions.get(node.get("peer").asText());
            if (peerSession == null) {
                session.getAsyncRemote().sendText("{ \"error\" : \"No peer found\"}");
                return;
            }
            ((ObjectNode)node).put("peer", session.getId());
            peerSession.getAsyncRemote().sendText(mapper.writeValueAsString(node));
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
	}


    private synchronized void sendIdMessage(Session session, String type, String id) {
        String textMessage = "{ \"" + type + "\" : \"" + id + "\"}";
        session.getAsyncRemote().sendText(textMessage);
    }

	@OnClose
	public void onClose(Session session) {
	    sessions.remove(session);
	}
}
